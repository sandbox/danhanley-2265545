var ActiveStandards = function(statusReport, util) {
    
    this.statusReport = statusReport;
    this.util = util;
    this.currentCheckpoint = null;

    /**
     * Construct preview using report data
     */
    this.init = function() {
        var checkpoints = this.getFailedCheckpoints(statusReport);
        if (checkpoints) {
            util.renderTemplate('ast_template_splitpane');
            this.setupCheckpointErrorsList(checkpoints);
            this.setupClickHandlers();
        } else {
            util.renderTemplate('ast_template_perfect_page');
        }
    };
    
    /**
     * Sets up checkpoint errors in left hand side
     */
    this.setupCheckpointErrorsList = function(checkpoints) {

        var liHtml = '';
        for (var i=0; i<checkpoints.length; i++) {
            var c = checkpoints[i];
            var tpl = jQuery('#ast_checkpoint_li').html();
            liHtml += util.replacer(tpl, {
                'ID': c.id,
                'NAME': this.util.escapeHtml(c.reference + ' ' + c.name)
            });
        }

        var tot = checkpoints.length;
        var errorTxt = 'error' + (tot===1 ? '' : 's');
        jQuery('#ast_error_count').html(tot + ' ' + errorTxt + ' found:');
        jQuery('#ast_error_list').html(liHtml);
    };

    this.getFailedCheckpoints = function(statusReport) {
        if (statusReport.checkpoints) {
            return jQuery.grep(statusReport.checkpoints, function(checkpoint) {
                return checkpoint.failed;
            });
        }
        return [];
    };
    
    this.setupClickHandlers = function() {
        var me = this;
        jQuery(document).delegate('#ast_error_list a', 'click', function(ev){
            ev.preventDefault();
            me.handleCheckpointClick(this);
            return false;
        });
        jQuery(document).delegate('#page_view', 'click', function(ev){
            ev.preventDefault();
            me.setupPagePreview('page');
            return false;
        });
        jQuery(document).delegate('#source_view', 'click', function(ev){
            ev.preventDefault();
            me.setupPagePreview('source');
            return false;
        });
    };
    
    /**
     * Sets up right hand side view after checkpoint is clicked in left side
     */
    this.handleCheckpointClick = function(anchor) {

        var checkpoints = jQuery.grep(statusReport.checkpoints, function(c) {
            return c.id === jQuery(anchor).data('id');
        });
        this.currentCheckpoint = checkpoints[0];

        jQuery('#ast_error_list a').removeClass('ast_current_checkpoint');
        jQuery(anchor).addClass('ast_current_checkpoint');
        jQuery('#ast_start_message').hide(); // clear first time message
        jQuery('#ast_preview_header').show();

        var defaultView = this.getDefaultView(this.currentCheckpoint);
        this.setupPagePreview(defaultView);
    };

    /**
     * Sets up the right hand side iframe to display highlighted page content.
     * The passed in view parameter should be one of: page, source or none
     */
    this.setupPagePreview = function(view) {

        var checkpoint = this.currentCheckpoint;
        var iframe = jQuery('#ast_preview_iframe');
        jQuery('.toggle').hide(); // hide togglers

        var ch = checkpoint.canHighlight || {};
        var canHighlightAnything = ch && (ch.page || ch.source);
        if (!canHighlightAnything) {
            jQuery('#no_highlight_view').show();
        }
        
        var url = '#BASE#?q=activestandards/error/#ASSET#/checkpoint/#CHECKPOINT#/highlightSource/#VIEW#';
        var path = util.replacer(url, {
            'BASE': jQuery('#basePath').html(),
            'ASSET': statusReport.assetId,
            'CHECKPOINT': checkpoint.id,
            'VIEW': view
        });

        iframe.attr('src', path);
        iframe.show();
        
        this.setupPreviewToggle(checkpoint, view);
    };

    /**
     * Sets up the correct toggle option in the top toolbar, e.g. "switch to source view"
     */
    this.setupPreviewToggle = function(checkpoint, currentView) {
        var canHighlight = checkpoint.canHighlight || {};
        if ('page' === currentView && canHighlight.source) {
            jQuery('#source_view').show();
        } else if ('source' === currentView && canHighlight.page) {
            jQuery('#page_view').show();
        }    
    };

    this.getDefaultView = function(checkpoint) {
        var canHighlight = checkpoint.canHighlight || {};
        if (canHighlight.page) {
            return 'page';
        } else if (canHighlight.source) {
            return 'source';
        } else {
            return 'none';
        }
    };

};

ActiveStandards.Util = function() {
    
    /**
     * Lightweight templating: renders content of a text/html <script> tag
     */
    this.renderTemplate = function(templateId, props) {
        var tpl = jQuery('#' + templateId).html();
        jQuery('#ast_preview').html(tpl);
        props = props || {};
        for(id in props) {
            if (props.hasOwnProperty(id)) {
                jQuery('#' + id).html(props[id]);
            }
        }
    };
    
    this.replacer = function(text, props) {
        text = text || '';
        for(id in props) {
            if (props.hasOwnProperty(id)) {
                text = text.replace('#'+id+'#', props[id]);
            }
        }
        return text;
    };
    
    this.escapeHtml = function(text) {
        return !text ? '' : text.replace(/&/g,'&amp;').replace(/</g,'&lt;').replace(/>/g,'&gt;');
    };
    
    this.renderError = function(jqXHR) {
        
        var statusCode;
        var message;
        var err = {};
        var data = jqXHR ? jqXHR.responseText : '';
        
        if (data) {
            try {
                err = JSON.parse(data) || {};
            } catch (ex) {
                err.message = data; // in case error not JSON
            }
        }
        statusCode = err.statusCode || 'Unknown';
        message = err.message || 'No error details available';

        this.renderTemplate('ast_template_sys_error', {
            'ast_status_code': statusCode,
            'ast_error_message': message
        });
    };
    
};

(function(jQuery) {
    jQuery(document).ready(function () {

        var util = new ActiveStandards.Util();
        util.renderTemplate('ast_template_loading');
        
        var basePath = jQuery('#ast_base_path').html();
        var previewId = jQuery('#ast_preview_id').html();
        
        jQuery.ajax({
            // Using ?q= form, because not everyone using clean URLs
            url: basePath + '?q=activestandards/' + previewId,
            dataType: 'json',
            success: function(statusReport) {
                if (statusReport) {
                    var ast = new ActiveStandards(statusReport, util);
                    ast.init();
                } else {
                    util.renderError();
                }
            },
            error: function(jqXHR) {
                util.renderError(jqXHR);
            }
        });
        
    });
})(jQuery);