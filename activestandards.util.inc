<?php

define('ACTIVESTANDARDS_PARAM_URL', 'activestandards_api_url');
define('ACTIVESTANDARDS_PARAM_KEY', 'activestandards_api_key');
define('ACTIVESTANDARDS_PARAM_DEFAULT_WEBSITE', 'activestandards_api_default_website');
define('ACTIVESTANDARDS_DEFAULT_API_BASE_URL', 'http://api.activestandards.com/v1');
define('ACTIVESTANDARDS_PATH', base_path() . drupal_get_path('module', 'activestandards'));
define('ACTIVESTANDARDS_WATCHDOG_TAG', 'activestandards');
define('ACTIVESTANDARDS_CHECK_BUTTON_TEXT', 'Check with ActiveStandards');

function activestandards_get_base_url() {
    $vars = activestandards_get_vars();
    return $vars['baseUrl'];
}

function activestandards_get_api_key() {
    $vars = activestandards_get_vars();
    return $vars['apiKey'];
}

function activestandards_get_vars() {
    return array(
        'baseUrl' => variable_get(ACTIVESTANDARDS_PARAM_URL),
        'apiKey' => variable_get(ACTIVESTANDARDS_PARAM_KEY)
    );
}

/**
 * @return true if the API is ready to use, i.e. a URL and key have been set
 */
function activestandards_api_ready() {

    $wtype = ACTIVESTANDARDS_WATCHDOG_TAG;
    
    $apiUrl = variable_get(ACTIVESTANDARDS_PARAM_URL);
    if ($apiUrl == null || strlen($apiUrl) == 0) {
        //watchdog($wtype, "ActiveStandards API base URL undefined"); //cauton: may be verbose
        return false;
    }

    $apiKey = variable_get(ACTIVESTANDARDS_PARAM_KEY);
    if ($apiKey == null || strlen($apiKey) == 0) {
        //watchdog($wtype, "ActiveStandards API key undefined"); //cauton: may be verbose
        return false;
    }

    $defaultWebsiteId = variable_get(ACTIVESTANDARDS_PARAM_DEFAULT_WEBSITE);
    if ($defaultWebsiteId == null || strlen($defaultWebsiteId) == 0) {
        //watchdog($wtype, "ActiveStandards default website has not been set"); //cauton: may be verbose
        return false;
    }

    return true;
}

/**
 * Tries to detect the base URL
 */
function activestandards_detect_baseurl($file = 'install.php') {
    global $base_url;
    if ($base_url != null && strlen($base_url) > 0) {
        return $base_url;
    } else {
        // Copy of drupal_detect_baseurl() in install.inc 
        $proto = $_SERVER['HTTPS'] ? 'https://' : 'http://';
        $host = $_SERVER['SERVER_NAME'];
        $port = ($_SERVER['SERVER_PORT'] == 80 ? '' : ':' . $_SERVER['SERVER_PORT']);
        $uri = preg_replace("/\?.*/", '', $_SERVER['REQUEST_URI']);
        $dir = str_replace("/$file", '', $uri);
        return "$proto$host$port$dir";
    }
}

function activestandards_parse_json_response($response) {
    if (isset($response)) {
        $data = $response->data;
        if (isset($data)) {
            $jsonData = drupal_json_decode($data);
            if (!isset($jsonData)) {
                activestandards_log("No JSON response, instead: " . $response->data);
            }
            return $jsonData;
        } 
    }
    return null;
}

function activestandards_get_header($response, $headerName) {
    if ($response->headers != null) {
        return $response->headers[$headerName];
    }
    return null;
}

function activestandards_get_html_template($templateFilename) {
    $html = activestandards_get_template($templateFilename);
    $html = str_replace('#AST_PATH#', ACTIVESTANDARDS_PATH, $html);
    return $html;
}

/**
 * Loads a template file from the file system
 * 
 * @param type $templateFilename
 * @return file contents
 */
function activestandards_get_template($templateFilename) {
    $filePath = drupal_get_path('module', 'activestandards') . $templateFilename;
    return file_get_contents($filePath);
}

/**
 * Return a preview object from cache.
 *
 * @param string $previewId
 *   The cache key for the object to be retrieved.
 *
 * @return object
 *   The cached object, or FALSE if none was found.
 */
function activestandards_cache_get($previewId) {
    $cache = cache_get($previewId, 'cache_activestandards');
    return ($cache) ? $cache->data : FALSE;
}

function activestandards_log($msg) {
    watchdog(ACTIVESTANDARDS_WATCHDOG_TAG, $msg);
}

function activestandards_log_error($response) {
    $error = activestandards_parse_error($response);
    activestandards_log("ActiveStandards error: " . $error['statusCode'] . ": " . $error['message']);
}

function activestandards_parse_error($response) {
    
    $error = array();
    $jsonResp = activestandards_parse_json_response($response);
    
    if (isset($jsonResp) && isset($jsonResp['statusCode'])) {
        $error['statusCode'] = $jsonResp['statusCode'];
        $error['message'] = $jsonResp['message'];
    } else {
        // Provide a default code and message
        // (Jira JAM-4128: strip_tags to remove <h1> from non-JSON 596 error response, until fix is in)
        $error['statusCode'] = isset($response->code) ? $response->code : "internal";
        $error['message'] = isset($response->data) ? strip_tags($response->data) : "No message about the error was returned";
    }
    return $error;
}

function activestandards_create_system_error_page($response) {
    activestandards_log_error($response);
    $error = activestandards_parse_error($response);
    return activestandards_make_system_error_page($error['statusCode'], $error['message']);
}

function activestandards_make_system_error_page($statusCode, $message) {
    $html = activestandards_get_html_template('/activestandards.system.error.html');
    $html = str_replace("#STATUS_CODE#", $statusCode, $html);
    $html = str_replace("#ERROR_MESSAGE#", $message, $html);
    return $html;
}