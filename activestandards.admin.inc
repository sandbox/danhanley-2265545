<?php

/* = = = = = =  Core Config  = = = = = = = */

function activestandards_admin_settings_core() {
    
    $form[ACTIVESTANDARDS_PARAM_URL] = array(
        '#type' => 'textfield',
        '#size' => 60,
        '#title' => 'API Base URL',
        '#default_value' => variable_get(ACTIVESTANDARDS_PARAM_URL, ACTIVESTANDARDS_DEFAULT_API_BASE_URL),
        '#description' => t('Set this to the base URL of the ActiveStandards API')
    );
    
    $form[ACTIVESTANDARDS_PARAM_KEY] = array(
        '#type' => 'textfield',
        '#size' => 60,
        '#title' => 'Your API Key',
        '#default_value' => variable_get(ACTIVESTANDARDS_PARAM_KEY),
        '#description' => t('Set this to your API key, as listed in your account')
    );
    
    $form['#submit'][] = 'activestandards_admin_settings_core_submit';

    return system_settings_form($form);
}

function activestandards_admin_settings_core_validate($form, &$formState) {
    $url = $formState['values'][ACTIVESTANDARDS_PARAM_URL];
    if ($url == null || strlen($url) == 0) {
        form_set_error(ACTIVESTANDARDS_PARAM_URL, t('Please enter a base URL'));
    }
    
    $apiKey = $formState['values'][ACTIVESTANDARDS_PARAM_KEY];
    if ($apiKey == null || strlen($apiKey) == 0) {
        form_set_error(ACTIVESTANDARDS_PARAM_KEY, t('Please enter your API Key. API calls will not be accepted without this.'));
    }
}

function activestandards_admin_settings_core_submit($form, &$formState) {
    
    $url = $formState['values'][ACTIVESTANDARDS_PARAM_URL];
    variable_set(ACTIVESTANDARDS_PARAM_URL, $url);
    
    $apiKey = $formState['values'][ACTIVESTANDARDS_PARAM_KEY];
    variable_set(ACTIVESTANDARDS_PARAM_KEY, $apiKey);
}


/* = = = = = =  Websites Config  = = = = = = = */

function activestandards_admin_settings_websites() {
    
    $msg = t('Set this to the ID of an ActiveStandards Website entity. ' . 
            'The choice of website determines which checkpoints are used ' . 
            'during quality checking of your content.');
    
    $form[ACTIVESTANDARDS_PARAM_DEFAULT_WEBSITE] = array(
        '#type' => 'textfield',
        '#size' => 60,
        '#title' => 'Default Website ID',
        '#default_value' => variable_get(ACTIVESTANDARDS_PARAM_DEFAULT_WEBSITE),
        '#description' => $msg
    );
    $form['#submit'][] = 'activestandards_admin_settings_websites_submit';

    return system_settings_form($form);
}

function activestandards_admin_settings_websites_validate($form, &$formState) {
    $websiteId = $formState['values'][ACTIVESTANDARDS_PARAM_DEFAULT_WEBSITE];
    if ($websiteId == null || strlen($websiteId) == 0) {
        form_set_error(ACTIVESTANDARDS_PARAM_DEFAULT_WEBSITE, t('Please enter a default Website ID'));
    }
}

function activestandards_admin_settings_websites_submit($form, &$formState) {
    $websiteId = $formState['values'][ACTIVESTANDARDS_PARAM_DEFAULT_WEBSITE];
    variable_set(ACTIVESTANDARDS_PARAM_DEFAULT_WEBSITE, $websiteId);
}